package sayand.modules.libcommand;

/**
 * 
 *
 */

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.android.LogcatAppender;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.logging.Level;





/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author espen
 */
public class Command {

        private static org.slf4j.Logger logger =
                LoggerFactory.getLogger(Command.class.getName());
        private String command;
        private ArrayList<String> stdouts = null;
        private ArrayList<String> stderrs = null;

    public static final String MSG_TAG = "TETHER -> Command ";

        public Command(String command) {
                this.command = command;
        }

        public static void closeStream(Closeable closeable) {
                if (null != closeable) {
                        try {
                                closeable.close();
                        } catch (IOException ex) {
                                logger.warn(MSG_TAG+"Failed to properly close closeable." + ex);
                        }
                }
        }

        class BufferedReaderThread extends Thread {

                BufferedReader stdOutBufReader;

                BufferedReaderThread(BufferedReader stdOutBufReader) {
                        this.stdOutBufReader = stdOutBufReader;
                }

                @Override
                public void run() {

                        String s = null;



                        try {

                                while ((s = stdOutBufReader.readLine()) != null) {
                                        stdouts.add(s);
                                }
                        } catch (IOException e) {
                                e.printStackTrace();
                        }
                }
        }

        public synchronized int excecute(String pipeBuffer) throws IOException {
                int exitValue = -1;
                String s = null;

                stdouts = new ArrayList<String>();
                stderrs = new ArrayList<String>();



                Process Process = null;
                OutputStream stdin = null;
                InputStream stdout = null;
                OutputStreamWriter osStdIn = null;
                InputStreamReader isStdout = null;
                InputStream stderr = null;
                InputStreamReader isStderr = null;

                BufferedReader stdOutBufReader = null;
                BufferedReader stdErrBufReader = null;
                BufferedWriter stdoutBufWriter = null;




                try {

                        // run the command
                        // using the Runtime exec method:
                        Process = Runtime.getRuntime().exec(getCommand());

                        /*
            Get the output stream of the subprocess. Output to the stream is
            piped into the standard input stream of the process represented
            by this Process object.
                         * 
                         */
                        stdin = Process.getOutputStream();

                        /*
            Get the input stream of the subprocess. The stream obtains data piped
            from the standard
            output stream of the process represented by this Process object.
                         *
                         */
                        stdout = Process.getInputStream();


                        /*
            Gets the error stream of the subprocess. The stream obtains data piped
                         * from the error output stream of the process represented by this
                         * Process object.
                         */
                        stderr = Process.getErrorStream();

                        osStdIn = new OutputStreamWriter(stdin);
                        isStdout = new InputStreamReader(stdout);
                        isStderr = new InputStreamReader(stderr);

                        stdOutBufReader = new BufferedReader(isStdout);
                        stdErrBufReader = new BufferedReader(isStderr);
                        stdoutBufWriter = new BufferedWriter(osStdIn);


                        if (pipeBuffer != null) {
                                try {
                                        pipe(stdoutBufWriter, pipeBuffer);

                                } catch (IOException e) {
                                        e.printStackTrace();
                                }
                        }

                        // read the standard output from the command
                        while ((s = stdOutBufReader.readLine()) != null) {
                            System.out.println("stdout read: " + s);
                            stdouts.add(s);
                        }

                        // read  stderrors from the  command
                        while ((s = stdErrBufReader.readLine()) != null) {
                            System.out.println("stderr read: " + s);
                            stderrs.add(s);
                        }

                        exitValue = Process.waitFor();




                } catch (IOException e) {
                        logger.debug(MSG_TAG+"IO Error", e);
                        exitValue = -1;
                } catch (InterruptedException e) {
                        logger.debug(MSG_TAG+"InterruptedException",e);
                        exitValue = -1;
                } finally {
                        stdin.close();
                        stderr.close();
                        stdout.close();
                        stdin = null;
                        stderr = null;
                        stdout = null;

                        osStdIn.close();
                        isStdout.close();
                        isStderr.close();
                        osStdIn = null;
                        isStdout = null;
                        isStderr = null;

                        stdOutBufReader.close();
                        stdErrBufReader.close();
                        stdoutBufWriter.close();
                        stdOutBufReader = null;
                        stdErrBufReader = null;
                        stdoutBufWriter = null;
                        Process.destroy();
                        Process = null;

                }
                return exitValue;
        }

        public void pipe(BufferedWriter out, String msg) throws IOException {

                try {
                        out.write(msg + "\n");
                        out.flush();

                } catch (IOException err) {
                        err.printStackTrace();
                        throw err;
                }

        }

        /**
         * @return the command
         */
        public String getCommand() {
                return command;
        }

        /**
         * @param command the command to set
         */
        public void setCommand(String command) {
                this.command = command;
        }

        /**
         * @return the stdouts
         */
        public ArrayList<String> getStdouts() {
                return stdouts;
        }

        /**
         * @param stdouts the stdouts to set
         */
        public void setStdouts(ArrayList<String> stdouts) {
                this.stdouts = stdouts;
        }

        /**
         * @return the stderrs
         */
        public ArrayList<String> getStderrs() {
                return stderrs;
        }

        /**
         * @param stderrs the stderrs to set
         */
        public void setStderrs(ArrayList<String> stderrs) {
                this.stderrs = stderrs;
        }

        public static void printList2Stdout(ArrayList<String> l) {
                for (int i = 0; i < l.size(); i++) {
                        System.out.println((String) l.get(i));
                }

        }

        public void printList2StdoutN(ArrayList<String> l) {
                for (int i = 0; i < l.size(); i++) {
                        System.out.println((String) l.get(i));
                }

        }

        public String stdoutToString() {
                String ret = "";
                for (int i = 0; i < stdouts.size(); i++) {
                        ret += stdouts.get(i);
                }
                return ret;
        }
        
        
        //Test
        /*
        public static void main(String args[]) {

                Command command = new Command("/usr/bin/gnuplot /home/espen/Dropbox/gnuplot/test5.plt");
                try {
                        int ret = command.excecute(null);
                } catch (IOException e) {
                        e.printStackTrace();
                        
                }
                
                ArrayList<String> stdouts = command.getStdouts();
                ArrayList<String> stderrs = command.getStderrs();
                System.out.println("Here is the standard output of the command:");
                printList2Stdout(stdouts);
                System.out.println("Here is the standard errrors of the command:");
                printList2Stdout(stderrs);
        }
        */

}