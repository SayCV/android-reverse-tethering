package sayand.modules.libcommand;

import java.io.IOException;
import java.util.ArrayList;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for libcommand.
 */
public class CommandTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public CommandTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( CommandTest.class );
    }

    /**
     * Test return value
     * @throws IOException 
     */
    public void testCommand() throws IOException
    {
            Command command = new Command("dir");
                try {
                        int ret = command.excecute(null);
                        assertTrue(ret == 0 ) ;
                } catch (IOException e) {
                        throw e;
                        
                }
    }
    
    public void testStdout() throws IOException
    {
            Command command = new Command("dir");
                try {
                        int ret = command.excecute(null);
                        
                        String stdOutput = list2String(command.getStdouts());
                        System.out.println("Here is the standard output of the command: " 
                                                                + stdOutput );
                        assertTrue( ret == 0 );
                         assertTrue(stdOutput.contains("pom.xml"));
                        
                } catch (IOException e) {
                        throw e;
                        
                }
    }
    
    public void testErr() throws IOException
    {
            Command command = new Command("dir IDontExist");
                try {
                        int ret = command.excecute(null);
                        String stdError = list2String(command.getStderrs());
                        System.out.println("Here is the standard errors of the command: " 
                                        + stdError );
                        assertTrue(stdError.length() > 0);
                        assertTrue( ret != 0 );
                        
                } catch (IOException e) {
                        throw e;
                        
                }
    }
    
    
    public void testStress() throws IOException
    {
            Command command = new Command("dir");
                int count = 1000;
                System.out.println("Launching command "+ count + " times .. ");
            try {
                        
                        for ( int i = 0; i < count; i++ ) {
                                int ret = command.excecute(null);
                                assertTrue( ret == 0 );
                        }
                } catch (IOException e) {
                        throw e;
                        
                }
    }
    
    public static String list2String(ArrayList<String> l) {
                String ret = "";
            for (int i = 0; i < l.size(); i++) {
                        ret +=  l.get(i);
                }
            return ret;
            
        }
}