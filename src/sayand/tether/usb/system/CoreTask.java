/**
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation; either version 3 of the License, or (at your option) any later 
 *  version.
 *  You should have received a copy of the GNU General Public License along with 
 *  this program; if not, see <http://www.gnu.org/licenses/>. 
 *  Use this application at your own risk.
 *
 *  Copyright (c) 2009 by Harald Mueller and Seth Lemons.
 */

package sayand.tether.usb.system;

import sayand.tether.usb.MainActivity;
import sayand.tether.usb.data.ClientData;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.concurrent.TimeoutException;
import java.util.zip.GZIPInputStream;

import android.content.Context;
import android.util.Log;

import org.slf4j.LoggerFactory;
import org.sufficientlysecure.rootcommands.RootCommands;
import org.sufficientlysecure.rootcommands.Shell;
import org.sufficientlysecure.rootcommands.Toolbox;
import org.sufficientlysecure.rootcommands.command.Command;
import org.sufficientlysecure.rootcommands.command.SimpleCommand;
import org.sufficientlysecure.rootcommands.command.SimpleExecutableCommand;
import org.sufficientlysecure.rootcommands.util.BrokenBusyboxException;

public class CoreTask {

    public static final org.slf4j.Logger logger = LoggerFactory.getLogger(MainActivity.class);
	public static final String MSG_TAG = "TETHER -> CoreTask ";
	
	public String DATA_FILE_PATH;
    public String SQLITE3_BIN_PATH;

    /**
     * 第一次获取root权限后，push 到 /system/bin/ 目录下的 二进制可执行程序 的名称
     */
    public String ROOT_SU;
    public static final String RETURN_CODE_ERROR = "return_code_error";
	
	private static final String FILESET_VERSION = "1";
	//private static final String defaultDNS1 = "208.67.220.220";
	public	static final String SETTING_DB_PATH = "/data/data/com.android.providers.settings/databases/";
	public  static final String secureSetting_tether_supported = "tether_supported"; //valid setting is 1
	public  static final String secureSetting_tether_dun_required = "tether_dun_required"; //valid setting is 0
	private static final String defaultDNS1 = "8.8.4.4"; //use google dns as dns1 default
	private static final String defaultDNS2 = "8.8.8.8"; //use google dns as dns2 default	
	public static final String DEFAULT_NETWORK = "192.168.0.252";
	public static final String DEFAULT_GATEWAY = "192.168.0.1";
	public static final String USB_NETMASK = "255.255.255.0";
	private Hashtable<String,String> runningProcesses = new Hashtable<String,String>();

    public void setRootSu(String name){
        this.ROOT_SU = name;
    }

	public void setPath(String path){
		this.DATA_FILE_PATH = path;
	}

    public void setSqlite3Path(boolean hasSqlite3){
        if(hasSqlite3) {
            this.SQLITE3_BIN_PATH = "";
        } else {
            this.SQLITE3_BIN_PATH = this.DATA_FILE_PATH;
        }
    }
    
	
    public Hashtable<String,ClientData> getLeases() throws Exception {   //not used in reverse tethering
        Hashtable<String,ClientData> returnHash = new Hashtable<String,ClientData>();
        
        ClientData clientData;
        
        ArrayList<String> lines = readLinesFromFile(this.DATA_FILE_PATH+"/var/dnsmasq.leases");
        
        for (String line : lines) {
			clientData = new ClientData();
			String[] data = line.split(" ");
			String macAddress = data[1];
			String ipAddress = data[2];
			String clientName = data[3];
			clientData.setClientName(clientName);
			clientData.setIpAddress(ipAddress);
			clientData.setMacAddress(macAddress);
			returnHash.put(macAddress, clientData);
		}
    	return returnHash;
    }
 
    public boolean chmodBin() {
    	if (NativeTask.runCommand("chmod 0755 "+this.DATA_FILE_PATH+"/bin/*") == 0) {
    		return true;
    	}
    	return false;
    }   
    
    public ArrayList<String> readLinesFromFile(String filename) {
    	String line = null;
    	BufferedReader br = null;
    	InputStream ins = null;
    	ArrayList<String> lines = new ArrayList<String>();
    	//logger.debug(MSG_TAG+"Reading lines from file: " + filename);
        Log.d(MSG_TAG,"Reading lines from file: " + filename);
    	//appendLog(MSG_TAG + "Reading lines from file: " + filename); //added temp
    	try {
    		ins = new FileInputStream(new File(filename));
    		br = new BufferedReader(new InputStreamReader(ins), 8192);
    		while((line = br.readLine())!=null) {
    			lines.add(line.trim());
    		}
    	} catch (Exception e) {
            Log.d(MSG_TAG,"Unexpected error - Here is what I know: "+e.getMessage());
    		//appendLog(MSG_TAG +"Unexpected error - Here is what I know: "+e.getMessage()); //added temp
    	}
    	finally {
    		try {
    			ins.close();
    			br.close();
    		} catch (Exception e) {
    			// Nothing.
    		}
    	}
    	return lines;
    }
    
    public boolean writeLinesToFile(String filename, String lines) {
		OutputStream out = null;
		boolean returnStatus = false;
		logger.debug(MSG_TAG+"Writing " + lines.length() + " bytes to file: " + filename);
		try {
			out = new FileOutputStream(filename);
        	out.write(lines.getBytes());
		} catch (Exception e) {
			logger.debug(MSG_TAG+"Unexpected error - Here is what I know: "+e.getMessage());
		}
		finally {
        	try {
        		if (out != null)
        			out.close();
        		returnStatus = true;
			} catch (IOException e) {
				returnStatus = false;
			}
		}
		return returnStatus;
    }
    
    public synchronized void appendLog(String text)
    {
       MainActivity.logger.debug(text);
       String logFileName = this.DATA_FILE_PATH+"/var/tether.log"; 
       File logFile = new File(logFileName);
       if (!logFile.exists())
       {
          try
          {
             logFile.createNewFile();
          } 
          catch (IOException e)
          {
             // TODO Auto-generated catch block
             e.printStackTrace();
          }
       }
       try
       {
          //BufferedWriter for performance, true to set append to file flag
          BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true)); 
          buf.append(text);
          buf.newLine();
          buf.close();
       }
       catch (IOException e)
       {
          // TODO Auto-generated catch block
          e.printStackTrace();
       }
    }

    public synchronized void clearLog( )
    {       
       String logFileName = this.DATA_FILE_PATH+"/var/tether.log"; 
       File logFile = new File(logFileName);
       if (!logFile.exists())
       {
          try
          {
             logFile.createNewFile();
          } 
          catch (IOException e)
          {
             // TODO Auto-generated catch block
             e.printStackTrace();
          }
       }
       try
       {
          //BufferedWriter for performance, true to set append to file flag
          BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, false)); 
          buf.append(null);
          buf.newLine();
          buf.close();
       }
       catch (IOException e)
       {
          // TODO Auto-generated catch block
          e.printStackTrace();
       }
    }

    
    
    public boolean isNatEnabled() {
    	ArrayList<String> lines = readLinesFromFile("/proc/sys/net/ipv4/ip_forward");
    	return lines.contains("1");
    }
    //added by jason 14April2012
    public boolean isUsbNetEnabled() {
    	boolean usbNetStatus = false;
    	boolean usbNetStatus1 = false;
    	boolean usbNetStatus2 = false;
    	boolean usbNetStatus3 = false;
        boolean usbNetStatus4 = false; //added by sayCV 20131206
        boolean usbNetStatus5 = false; //added by sayCV 20131206
        boolean usbNetStatus6 = false; //added by sayCV 20131206

    	if ((new File("/sys/devices/virtual/net/usb0/enable")).exists()){
     	ArrayList<String> lines1 = readLinesFromFile("/sys/devices/virtual/net/usb0/enable");
     	usbNetStatus1 = lines1.contains("1");
     	logger.debug(MSG_TAG+"lines1"+lines1);
    	}
    	
    	if ((new File("/sys/devices/virtual/usb_composite/rndis/enable")).exists()){
    	ArrayList<String> lines2 = readLinesFromFile("/sys/devices/virtual/usb_composite/rndis/enable");
    	usbNetStatus2= lines2.contains("1");
    	logger.debug(MSG_TAG+"lines2"+lines2);
    	}
    	
    	if ((new File("/sys/devices/platform/msm_hsusb/usb_function_switch")).exists()){
    	ArrayList<String> lines3 = readLinesFromFile("/sys/devices/platform/msm_hsusb/usb_function_switch");
    	usbNetStatus3= lines3.contains("4");
    	logger.debug(MSG_TAG+"lines3"+lines3);
    	}

        if ((new File("/sys/devices/virtual/android_usb/android0/f_rndis/on")).exists()){
            ArrayList<String> lines4 = readLinesFromFile("/sys/devices/virtual/android_usb/android0/f_rndis/on");
            usbNetStatus4= lines4.contains("1");
            logger.debug(MSG_TAG+"lines4"+lines4);
        }

        if ((new File("/sys/class/android_usb/f_rndis/on")).exists()){
            ArrayList<String> lines5 = readLinesFromFile("/sys/class/android_usb/f_rndis/on");
            usbNetStatus5= lines5.contains("1");
            logger.debug(MSG_TAG+"lines5"+lines5);
        }

        if ((new File("/sys/devices/virtual/android_usb/android0/f_rndis/device/enable")).exists()){
            ArrayList<String> lines6 = readLinesFromFile("/sys/devices/virtual/android_usb/android0/f_rndis/device/enable");
            usbNetStatus4= lines6.contains("1");
            logger.debug(MSG_TAG+"lines6"+lines6);
        }

    	usbNetStatus = usbNetStatus1|| usbNetStatus2|| usbNetStatus3|| usbNetStatus4|| usbNetStatus5|| usbNetStatus6;
    	return usbNetStatus;
    	
    }
    public String getKernelVersion() {
        ArrayList<String> lines = readLinesFromFile("/proc/version");
        String version = lines.get(0).split(" ")[2];
        logger.debug(MSG_TAG+"Kernel version: " + version);
        return version;
    }
    
    public synchronized boolean hasKernelFeature(String feature) {//never been called
    	try {
			FileInputStream fis = new FileInputStream("/proc/config.gz");
			GZIPInputStream gzin = new GZIPInputStream(fis);
			BufferedReader in = null;
			String line = "";
			in = new BufferedReader(new InputStreamReader(gzin));
			while ((line = in.readLine()) != null) {
				   if (line.startsWith(feature)) {
					    gzin.close();
						return true;
					}
			}
			gzin.close();
    	} catch (IOException e) {
    		//
    		logger.debug(MSG_TAG+"Unexpected error - Here is what I know: "+e.getMessage());
    	}
    	return false;
    }

    public boolean isProcessRunning(String processName) throws Exception {
    	boolean processIsRunning = false;
    	Hashtable<String,String> tmpRunningProcesses = new Hashtable<String,String>();
    	File procDir = new File("/proc");
    	FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                try {
                    Integer.parseInt(name);
                } catch (NumberFormatException ex) {
                    return false;
                }
                return true;
            }
        };
    	File[] processes = procDir.listFiles(filter);
    	for (File process : processes) {
    		String cmdLine = "";
    		// Checking if this is a already known process
    		if (this.runningProcesses.containsKey(process.getAbsoluteFile().toString())) {
    			cmdLine = this.runningProcesses.get(process.getAbsoluteFile().toString());
    		}
    		else {
    			ArrayList<String> cmdlineContent = this.readLinesFromFile(process.getAbsoluteFile()+"/cmdline");
    			if (cmdlineContent != null && cmdlineContent.size() > 0) {
    				cmdLine = cmdlineContent.get(0);
    			}
    		}
    		// Adding to tmp-Hashtable
    		tmpRunningProcesses.put(process.getAbsoluteFile().toString(), cmdLine);
    		
    		// Checking if processName matches
    		if (cmdLine.contains(processName)) {
    			processIsRunning = true;
    		}
    	}
    	// Overwriting runningProcesses
    	this.runningProcesses = tmpRunningProcesses;
    	return processIsRunning;
    }

    public boolean hasRootPermission() {
    	boolean rooted = true;
		try {
			File su = new File("/system/bin/su");
			if (!su.exists()) {
				su = new File("/system/xbin/su");
				if (!su.exists())
					rooted = false;
			}
		} catch (Exception e) {
			logger.debug(MSG_TAG+"Can't obtain root - Here is what I know: "+e.getMessage());
			rooted = false;
		}
		return rooted;
    }

    public boolean hasSqlite3() {
        boolean sqliteBin = true;
        try {
            File su = new File("/system/bin/sqlite3");
            if (!su.exists()) {
                su = new File("/system/xbin/sqlite3");
                if (!su.exists())
                    sqliteBin = false;
            }
        } catch (Exception e) {
            logger.debug(MSG_TAG+"Can't obtain sqlite3 - Here is what I know: "+e.getMessage());
            sqliteBin = false;
        }
        return sqliteBin;
    }

    public static void setReturnCode(String returnKey, String returnCode) {
        System.out.println(returnKey + returnCode);
    }
    public static String getReturnCode(String returnKey, String printf,String defaultReturnCode) {
        String resultCode = defaultReturnCode;
        if (!isEmpty(returnKey) & !isEmpty(printf)) {
            if (printf.startsWith(returnKey)) {
                int keyLen = returnKey.length();
                resultCode = printf.substring(keyLen, printf.length());
            }
        }
        return resultCode;
    }
    public static boolean isEmpty(Object obj) {
        boolean flag = true;
        if (obj != null) {
            if (obj instanceof String) {
                String s = (String) obj;
                if (!s.trim().equals("")) {
                    flag = false;
                }
            } else {
                flag = false;
            }
        }
        return flag;
    }
    /**
     * 执行命令progArray，同时通过returnKey来获取返回值 returnKey
     *
     * @param progArray
     * @param returnKey
     * @return
     */
    public static String exeCmd(String[] progArray, String returnKey) {
        DataOutputStream os = null;
        Process process = null;
        BufferedReader reader = null;
        String returnCode = RETURN_CODE_ERROR;
        for (String str : progArray) {
            System.out.println("progArray: "+ str);
        }
        try {
            process = Runtime.getRuntime().exec(progArray);
            os = new DataOutputStream(process.getOutputStream());
            os.flush();
            process.waitFor();
            reader = new BufferedReader(new InputStreamReader(
                    process.getInputStream()));
            String print = null;
            while ((print = reader.readLine()) != null) {
                System.out.println("print: " + print);
                returnCode = getReturnCode(returnKey, print,returnCode);
            }
        } catch (IOException e) {
            System.out.println("exeCmd IOException: " + e.getMessage());
            e.printStackTrace();
        } catch (InterruptedException e) {
            System.out
                    .println("exeCmd InterruptedException: " + e.getMessage());
            e.printStackTrace();
        } finally {

            try {
                if (os != null) {
                    os.close();
                }
                if (reader != null) {
                    reader.close();
                }
                if (process != null)
                    process.destroy();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return returnCode;
    }

    private class MyCommand extends Command {
        private StringBuilder sb = new StringBuilder();
        private int exitCode;
        int timeout = RootCommands.DEFAULT_TIMEOUT;

        public MyCommand(String... command) {
            super(command);
        }

        public MyCommand(int timeout, String... command) {
            super(timeout, command);
        }

        @Override
        public void output(int id, String line) {
            sb.append(line).append('\n');
        }

        @Override
        public void afterExecution(int id, int exitCode) {
            this.exitCode = exitCode;
        }

        public String getOutput() {
            return sb.toString();
        }

        public int getExitCode() {
            return this.exitCode;
        }


    }

    public boolean runRootCommand(String command) {
        logger.debug(MSG_TAG+"Root-Command ==> " + this.ROOT_SU + " -c \""+command+"\"");
        int returnCode = -1;
        try {
            //MyCommand binaryCommand = new MyCommand("su" + " -c \""+command+"\"", "");
            SimpleCommand binaryCommand = new SimpleCommand(command);

            // start root shell
            Shell shell = Shell.startRootShell();

            //shell.add(binaryCommand);
            shell.add(binaryCommand).waitForFinish();

            logger.debug(MSG_TAG+"Output of command: " + binaryCommand.getOutput());
            returnCode = binaryCommand.getExitCode();

            // close root shell
            shell.close();
        } catch (Exception e) {
            logger.error(MSG_TAG+"Exception!", e);
        }

        if (returnCode == 0) {
            return true;
        }
        logger.debug(MSG_TAG+"Root-Command error, return code: " + returnCode);
        return false;
    }

    public static String list2String(ArrayList<String> l) {
        String ret = "";
        for (int i = 0; i < l.size(); i++) {
            ret +=  l.get(i);
        }
        return ret;

    }

    public String getProp(String property) {
    	return NativeTask.getProp(property);
    }
    
    public long[] getDataTraffic(String device) {
    	// Returns traffic usage for all interfaces starting with 'device'.
    	long [] dataCount = new long[] {0, 0};
    	if (device == "")
    		return dataCount;
    	for (String line : readLinesFromFile("/proc/net/dev")) {
    		if (line.startsWith(device) == false)
    			continue;
    		line = line.replace(':', ' ');
    		String[] values = line.split(" +");
    		dataCount[0] += Long.parseLong(values[1]);
    		dataCount[1] += Long.parseLong(values[9]);
    	}
    	//logger.debug(MSG_TAG+"Data rx: " + dataCount[0] + ", tx: " + dataCount[1]);
        Log.d(MSG_TAG,"Data rx: " + dataCount[0] + ", tx: " + dataCount[1]);
    	return dataCount;
    }

    /*
     * Check the setting.txt for particular stringSetting, if it is there, return true, otherwise false
     */
    public boolean checkSecureSetting(String stringSetting) {
    	String secureSettingDumpedFile = this.DATA_FILE_PATH+"/setting.txt"; //this is the dumped secure table 
    	boolean settingIsDefined = false;
    	for (String line : readLinesFromFile(secureSettingDumpedFile)) {
    		if (line.contains(stringSetting) == true)
    			settingIsDefined = true;
    		
    	}
    	logger.debug(MSG_TAG+stringSetting+" inside global table is defined: " + settingIsDefined );
    	appendLog(MSG_TAG + stringSetting+" inside global table is defined: " + settingIsDefined);
    	return settingIsDefined;
    }    
    
    /*
     * check if particular stringSetting is enabled or not but without updating the settings.db
     * this function will run with the assumption that the checkSecureSetting is true.
     */
    public boolean secureSettingIsEnabled(String stringSetting, int iSetValue) {
    	boolean enabled = false;
    	String sSetValue = String.valueOf(iSetValue);
    	String settingLine = null;
    	String secureSettingDumpedFile = this.DATA_FILE_PATH+"/setting.txt"; //this is the dumped secure table 
    	for (String line : readLinesFromFile(secureSettingDumpedFile)) {
    		if (line.contains(stringSetting) && line.contains("\'"+sSetValue+"\'")){
    			enabled = true;
    			settingLine = line;
    		}   		
    	}
    	logger.debug(MSG_TAG+stringSetting+" inside global table is: " + settingLine );
    	return enabled;
    }    
    
    /*
     * parameter: setting to be enabled (has to pass the check setting first to use this function)
     * if enabled successfully, return true, otherwise return false
     */
    public synchronized boolean secureSettingEnable(String stringSetting, int iValue) {
    	boolean enabled = false;
    	String command;
    	String secureSettingFile = this.SETTING_DB_PATH+"settings.db"; //this is the dumped secure table 
    	command = this.SQLITE3_BIN_PATH + "sqlite3 "+ this.SETTING_DB_PATH + "settings.db "+ "\"update global set value ="+ String.valueOf(iValue) + "where name = " + "\'"+stringSetting +"\'"+"\"" ;
    	logger.debug(MSG_TAG+"command for enable secureSetting is : " + command);
    	if(runRootCommand(command)==false){
			logger.error(MSG_TAG+"Unable to enable the global Setting for the setting of" + stringSetting);
			return enabled;
		}; 
    	enabled = true;
		return enabled;
    }        
    
    /* 
     * need to define another function to get max(_id).
     * sqlite3 settings.db "select max(_id) from secure" >> maxid.txt
     * so the function will read from maxid.txt and return maxid
     * return -1 if fail otherwise return maxid.
     */
    public synchronized int secureSettingMaxId() {
    	int intMaxId = -1;
    	int intTmpId;
    	String command;
    	String secureSettingFile = this.SETTING_DB_PATH+"settings.db"; //this is the dumped secure table 
    	command = this.SQLITE3_BIN_PATH + "sqlite3 "+ this.SETTING_DB_PATH + "settings.db "+ "\'select max(_id) from global\'  > " + this.DATA_FILE_PATH + "/maxid.txt"; ;
    	logger.debug(MSG_TAG+"command for dump the max is: " + command);
    	if(runRootCommand(command)==false){
			logger.error(MSG_TAG+"Unable to get the global setting maxid from" + this.SETTING_DB_PATH+"/settings.db");
			appendLog(MSG_TAG + "Unable to get the global setting maxid from" + this.SETTING_DB_PATH+"/settings.db");;
			return -1;
		} 
		
		String secureSettingMaxIdDumpedFile = this.DATA_FILE_PATH+"/maxid.txt"; //this is the dumped secure table 
    	for (String line : readLinesFromFile(secureSettingMaxIdDumpedFile)) {
    		intTmpId = Integer.parseInt(line);
    		if (intTmpId > intMaxId)
    			intMaxId = intTmpId;
    		   		
    	}
    	logger.debug(MSG_TAG+"max Id in global setting table is" + intMaxId );
    	appendLog(MSG_TAG + "max Id in global setting table is" + intMaxId);
		return intMaxId;
    }        
    
    
    /*
     * parameters: setting to be inserted and intId to be used
     * return trun if succss, otherwise false
     * sqlite3 settings.db "insert into secure values(51,'test_insert',1 or 0)"
     */
    public synchronized boolean secureSettingInsertandEnable(String stringSetting, int intId, int iValue) {
    	boolean insertSuccess = false;
    	String command;

    	//String secureSettingFile = this.SETTING_DB_PATH+"settings.db"; //this is the dumped secure table 
    	command = this.SQLITE3_BIN_PATH + "sqlite3 "+ this.SETTING_DB_PATH + "settings.db "+ "\'insert into global values(" +String.valueOf(intId)+","+ "\\\""+ stringSetting +"\\\","+ String.valueOf(iValue) + ")\'" ;
    	logger.debug(MSG_TAG+"command to insert" + stringSetting +  "is :"  + command);
    	if(runRootCommand(command)==false){
			logger.error(MSG_TAG+"Unable to insert" + stringSetting + "global Setting for the setting of" + stringSetting);
			return false;
		} 
    	insertSuccess = true;
		return insertSuccess;
    }        
    
    /*
     * this function dumps the securesetting to a txt file. If success, return true, otherwise false
     * do not use new thread to run it now. 
     */
    public boolean dumpDefaultGW(){
		String dumpDefaultGW = this.DATA_FILE_PATH + "/bin/route  > " + this.DATA_FILE_PATH + "/conf/route.conf";
		logger.debug(MSG_TAG+"command for dumping the default gateway is: " + dumpDefaultGW);
		if(this.runRootCommand(dumpDefaultGW)==false){
			logger.error(MSG_TAG+"Unable to dump the route output to " + this.DATA_FILE_PATH+"/var/route.out");
			return false;
		} 
    	return true;
    }
    
    public String[] getCurrentGW( ){
    	String currentGWandIface[] = new String[2];
    	
    	String filename = this.DATA_FILE_PATH+"/conf/route.conf";
    	File inFile = new File(filename);
    	if (inFile.exists() == true) {
    		ArrayList<String> inputLines = readLinesFromFile(filename);
        	for (String line : inputLines) {
        		if (line.startsWith("default")) {
        			String[] routeOutPuts = line.split(" ");
        			currentGWandIface[0] = routeOutPuts[1];  //ip of gateway
        			currentGWandIface[1] = routeOutPuts[routeOutPuts.length-1]; //interface of gateway
        			break;
        		}
        		
        	}
    	}
    	
      	if (currentGWandIface[0] == null || currentGWandIface[0].length() <= 0) {
    		currentGWandIface[0] = "undefined";
    	}
      	if (currentGWandIface[1] == null || currentGWandIface[1].length() <= 0) {
    		currentGWandIface[1] = "undefined";
    	}
    	return currentGWandIface;
    }
    
    /*public synchronized void updateDnsmasqFilepath() {
    	String dnsmasqConf = this.DATA_FILE_PATH+"/conf/dnsmasq.conf";
    	String newDnsmasq = new String();
    	boolean writeconfig = false;
    	
    	ArrayList<String> lines = readLinesFromFile(dnsmasqConf);
    	
    	for (String line : lines) {
    		if (line.contains("dhcp-leasefile=") && !line.contains(CoreTask.this.DATA_FILE_PATH)){
    			line = "dhcp-leasefile="+CoreTask.this.DATA_FILE_PATH+"/var/dnsmasq.leases";
    			writeconfig = true;
    		}
    		else if (line.contains("pid-file=") && !line.contains(CoreTask.this.DATA_FILE_PATH)){
    			line = "pid-file="+CoreTask.this.DATA_FILE_PATH+"/var/dnsmasq.pid";
    			writeconfig = true;
    		}
    		newDnsmasq += line+"\n";
    	}

    	if (writeconfig == true)
    		writeLinesToFile(dnsmasqConf, newDnsmasq);
    }
    */
    public synchronized String[] getCurrentDns() {  
    	// Getting dns-servers
    	//jason GONG: this function is intended for getting the dns at the phone side network connection
    	//for tethering, for reverse tethering, I decided to use goolge's dns as the phone side
    	//dns server. 
    	String dns[] = new String[2];
    	dns[0] = getProp("net.dns1");
    	dns[1] = getProp("net.dns2");
    	if (dns[0] == null || dns[0].length() <= 0 || dns[0].equals("undefined")) {
    		dns[0] = defaultDNS1;
    	}
    	if (dns[1] == null || dns[1].length() <= 0 || dns[1].equals("undefined")) {
    		dns[1] = defaultDNS2;
    	}
    	return dns;
    }
    public synchronized String[] setCurrentDns() {  
    	// Setting dns-servers as google dns server, which is 8.8.4.4. and 8.8.8.8
    	String dns[] = new String[2];
    	String command;
    	dns[0] = defaultDNS1;
    	dns[1] = defaultDNS2;
    	command = "setprop net.dns1 "+defaultDNS1; 
		if(runRootCommand(command)==false){
			logger.error(MSG_TAG+"Unable to set net.dns1 as googles dns of 8.8.4.4.");
			dns[0] = "";
			return dns;
		}
		command = "setprop net.dns2 "+defaultDNS2; 
		if(runRootCommand(command)==false){
			logger.error(MSG_TAG+"Unable to set net.dns2 as googles dns of 8.8.8.8.");
			dns[1] = "";
			return dns;
		}
    	    	

    	return dns;
    }    
    public synchronized String[] updateResolvConf() {   
    	String resolvConf = this.DATA_FILE_PATH+"/conf/resolv.conf";
    	// Getting dns-servers
    	String dns[] = this.setCurrentDns();
    	String linesToWrite = new String();
    	linesToWrite = "nameserver "+dns[0]+"\n";
    	if (dns[1].length() > 0) {
    		linesToWrite += "nameserver "+dns[1]+"\n";
    	}
    	this.writeLinesToFile(resolvConf, linesToWrite);
    	return dns;
    }
    
    public boolean filesetOutdated(){
    	boolean outdated = true;
    	
    	File inFile = new File(this.DATA_FILE_PATH+"/conf/version");
    	if (inFile.exists() == false) {
    		return false;
    	}
    	ArrayList<String> lines = readLinesFromFile(this.DATA_FILE_PATH+"/conf/version");

    	int linecount = 0;
    	for (String line : lines) {
    		if (line.contains("@Version")){
    			String instVersion = line.split("=")[1];
    			if (instVersion != null && FILESET_VERSION.equals(instVersion.trim()) == true) {
    				outdated = false;
    			}
    			break;
    		}
    		if (linecount++ > 2)
    			break;
    	}
    	return outdated;
    }
 
    public long getModifiedDate(String filename) {
    	File file = new File(filename);
    	if (file.exists() == false) {
    		return -1;
    	}
    	return file.lastModified();
    }
    
    public String getLanIPConf() {  //possibly need to modify this for different ip address
    	String returnString = "192.168.0.252/30";
    	String filename = this.DATA_FILE_PATH+"/conf/lan_network.conf";
    	ArrayList<String> inputLines = readLinesFromFile(filename);
    	for (String line : inputLines) {
    		if (line.startsWith("network")) {
    			returnString = (line.split("=")[1])+"/30";
    			break;
    		}
    	}
    	return returnString;
    }
    
    
    /*
     * the readLanConf is almost the same as the above getLanIPConf.
     * the difference is that the getLANIPConf returns the 172.20.23.252/30
     * the readLanconf is more like the same name function defined in native cpp.
     * it returns the network and gateway as String[0] and String[1]
     */
    public String[] readLanConf(){
    	String network[] = new String[2];
    	String filename = this.DATA_FILE_PATH+"/conf/lan_network.conf";
    	network[0] = DEFAULT_NETWORK;
    	network[1] = DEFAULT_GATEWAY;
    	File inFile = new File(filename);
    	if (inFile.exists() == true) {
    		ArrayList<String> inputLines = readLinesFromFile(filename);
        	for (String line : inputLines) {
        		if (line.startsWith("network")) {
        			network[0] = line.split("=")[1];
        		}
        		else if (line.startsWith("gateway")){
        			network[1] = line.split("=")[1];
        		}
        		
        	}
    	}
    	return network;
    }
    
    public synchronized boolean writeLanConf(String lanconfString) { // need to update this function to remove the dns part.
    	boolean writesuccess = false;
    	
    	String filename = null;
    	//ArrayList<String> inputLines = null; removed for reverse tethering jason 13May2012
    	String fileString = null;
    	
    	// Assemble gateway-string
    	String[] lanparts = lanconfString.split("\\.");//split the lan setting string with "." Jason 13May2012
    	String gateway = lanparts[0]+"."+lanparts[1]+"."+lanparts[2]+".1";
    	
    	// Assemble dnsmasq dhcp-range //remove the ip range for reverse tethering
    	//String iprange = lanparts[0]+"."+lanparts[1]+"."+lanparts[2]+".253,"+lanparts[0]+"."+lanparts[1]+"."+lanparts[2]+".253,12h";
    	
    	// Update bin/tether
    	filename = this.DATA_FILE_PATH+"/conf/lan_network.conf";
       	fileString = "network="+lanparts[0]+"."+lanparts[1]+"."+lanparts[2]+".252\n";
       	fileString += "gateway="+gateway;

    	writesuccess = writeLinesToFile(filename, fileString);
    	if (writesuccess == false) {
    		logger.error(MSG_TAG+"Unable to update bin/tether with new lan-configuration.");
    		return writesuccess;
    	}
    	
    	// Update conf/dnsmasq.conf   //remove the dns setting part as it is not needed for reverser tethering
    	/*fileString = "";
    	filename = this.DATA_FILE_PATH+"/conf/dnsmasq.conf";
    	inputLines = readLinesFromFile(filename);   
    	for (String line : inputLines) {
    		
    		if (line.contains("dhcp-range")) {
    			line = "dhcp-range="+iprange;
    		}    		
    		fileString += line+"\n";
    	}
    	writesuccess = writeLinesToFile(filename, fileString);
    	if (writesuccess == false) {
    		logger.error(MSG_TAG+"Unable to update conf/dnsmasq.conf with new lan-configuration.");
    		return writesuccess;
    	} */   	
    	return writesuccess;
    }


	public boolean ifConfigUpInterface(String usbIface) {
		// TODO Auto-generated method stub
		String command;
    	command = this.DATA_FILE_PATH + "/bin/ifconfig "+ usbIface + " up" ; 
    	logger.debug(MSG_TAG+"command to up the " + usbIface + " is :"  + command);
    	this.appendLog(command);
    	if(runRootCommand(command)==false){
			logger.error(MSG_TAG+"Unable to up " + usbIface);
			this.appendLog("Unable to up " + usbIface);
			
			return false;
		} 
    	logger.debug(MSG_TAG+"----up " + usbIface +" success---" );
		return true;
	}


	public boolean ifConfigSetInterface(String usbIface, String [] network ) {
		// TODO Auto-generated method stub
		String command;
		command = this.DATA_FILE_PATH + "/bin/ifconfig " + usbIface + " " + network[0] + " netmask " + this.USB_NETMASK;  // right now the netmask is hard coded 
	  	logger.debug(MSG_TAG+"command to setup the " + usbIface + " is :"  + command);
    	this.appendLog(command);
    	if(runRootCommand(command)==false){
			logger.error(MSG_TAG+"Unable to setup " + usbIface + " with ip address of " + network[0]);
			this.appendLog("Unable to setup usbIface with ip address of " + network[0]);
			return false;
		} 
		return true;
	}


	public boolean ifConfigSetGW(String usbIface, String [] network) {
		// TODO Auto-generated method stub
		String command;
		command = this.DATA_FILE_PATH + "/bin/route add default gw " + network[1] + " " + usbIface;  // right now the netmask is hard coded 
	  	logger.debug(MSG_TAG+"command to setup the gateway of " + network[1] + " is :"  + command);
    	this.appendLog(command);
    	if(runRootCommand(command)==false){
			logger.error(MSG_TAG+"Unable to setup gateway with ip address of " + network[1]);
			this.appendLog("Unable to setup gateway with ip address of " + network[1]);
			return false;
		} 
		return true;
		
	}

    public boolean setSysUsbTether(boolean enable) {
        // Setting dns-servers as google dns server, which is 8.8.4.4. and 8.8.8.8
        String usbTetherOn, usbTetherOff;
        String command;
        usbTetherOn = "rndis,adb";
        usbTetherOff = "mtp,adb";
        if(enable) {
            command = "setprop sys.usb.config "+usbTetherOn;
        } else {
            command = "setprop sys.usb.config "+usbTetherOff;
        }
        if(runRootCommand(command)==false){
            logger.error(MSG_TAG+"Unable to set sys.usb.config: " +enable );
            return false;
        }
        return true;
    }

    public boolean ipConfigureMobileData(boolean enabled) {
        // TODO Auto-generated method stub
        String command;
        String rmnetIface = "rmnet0";
        String rmnetIpAddr = "0.0.0.0";

        if(enabled) {
            command = this.DATA_FILE_PATH + "/bin/ifconfig "+ rmnetIface + " " + rmnetIpAddr;//"up" ;
        } else {
            command = this.DATA_FILE_PATH + "/bin/ifconfig "+ rmnetIface + " " + "down" ;
        }
        logger.debug(MSG_TAG+"command to " + (enabled?"up ":"down ") + rmnetIface + " the is :"  + command);
        this.appendLog(command);
        if(runRootCommand(command)==false){
            logger.error(MSG_TAG+"Unable to " + (enabled?"up ":"down ") + rmnetIface);
            this.appendLog("Unable to " + (enabled?"up ":"down ") + rmnetIface);

            return false;
        }
        logger.debug(MSG_TAG+"----" + (enabled?"up ":"down ") + rmnetIface +" success---" );
        return true;
    }

    public void waitForFinish(int timeout) {
        synchronized (this) {
            while (true) {
                try {
                    this.wait(timeout);
                } catch (InterruptedException e) {
                    Log.e(RootCommands.TAG, "InterruptedException in waitForFinish()", e);
                }
                return;
            }
        }
    }
}
