package sayand.tether.usb;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import org.slf4j.LoggerFactory;

import sayand.tether.usb.R;

public class SetupPCHelpActivity extends Activity {
    public static final org.slf4j.Logger logger = LoggerFactory.getLogger(MainActivity.class);
	private Button closeHowto = null;
	private Button topApps = null; 
	public static final String MSG_TAG = "TETHER -> SetupPCHelpActivity";
	@Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.howtosetuppc);
		this.closeHowto = (Button) findViewById(R.id.closeHowto);
		this.topApps = (Button) findViewById(R.id.topApps);
		
		logger.debug(MSG_TAG+"close button for how to setuppc pressed ...");
        closeHowto.setOnClickListener(new closeHowtoListener());
        topApps.setOnClickListener(new topAppsListener());
        
		
	}
	
	class closeHowtoListener implements OnClickListener {
    	public void onClick(View v) {
          // do something when the button is clicked
        	logger.debug(MSG_TAG+"SetupPChelp pressed ...");
        	//Toast.makeText(v.getContext(), "Im Clicked!", Toast.LENGTH_SHORT).show();
    		//Intent i = new Intent(SetupPCHelpActivity.this, MainActivity.class);
    		//SetupPCHelpActivity.this.startActivity(i);
        	finish();
        }
    }
 
	class topAppsListener implements OnClickListener {
    	public void onClick(View v) {
          // do something when the button is clicked
        	logger.debug(MSG_TAG+"Top Apps pressed ...");
        	//Toast.makeText(v.getContext(), "Im Clicked!", Toast.LENGTH_SHORT).show();
    		//Intent i = new Intent(SetupPCHelpActivity.this, MainActivity.class);
    		//SetupPCHelpActivity.this.startActivity(i);
        	 String url = "http://ad.leadboltads.net/show_app_wall?section_id=989407946"; //YOUR LEADBOLT APPWALL ADRESS HERE!!
             Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
             startActivity(intent);
        	//finish();
        }
    }	
	

}
