/**
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation; either version 3 of the License, or (at your option) any later 
 *  version.
 *  You should have received a copy of the GNU General Public License along with 
 *  this program; if not, see <http://www.gnu.org/licenses/>. 
 *  Use this application at your own risk.
 *
 *  Copyright (c) 2012 by Jason GONG.
 */

package sayand.tether.usb;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.android.LogcatAppender;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.BatteryManager;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import sayand.tether.usb.R;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import android.R.drawable;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
//import android.os.SystemProperties;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;


import org.sufficientlysecure.rootcommands.RootCommands;
import org.sufficientlysecure.rootcommands.Shell;

public class MainActivity extends FragmentActivity {

    public static final org.slf4j.Logger logger = LoggerFactory.getLogger(MainActivity.class);

	private TetherApplication application = null;
	private ProgressDialog progressDialog;
    private DialogFragment newStartTetherFragment = null;
    private DialogFragment newStopTetherFragment = null;

	private ImageView startBtn = null;
	private ImageView stopBtn = null;
	private TextView progressTitle = null;
	private TextView progressText = null;
	private ProgressBar progressBar = null;
	private RelativeLayout downloadUpdateLayout = null;
	
	private RelativeLayout trafficRow = null;
	private TextView downloadText = null;
	private TextView uploadText = null;
	private TextView downloadRateText = null;
	private TextView uploadRateText = null;
	private Button setupComputer = null;
	
	private TableRow startTblRow = null;
	private TableRow stopTblRow = null;
	
	private ScaleAnimation animation = null;
	
	private static int ID_DIALOG_STARTING = 0;
	private static int ID_DIALOG_STOPPING = 1;


    public boolean usbActionDetectConnected = false;

	public static final int MESSAGE_NO_DATA_CONNECTION = 1;
	public static final int MESSAGE_CANT_START_TETHER = 2;
	public static final int MESSAGE_DOWNLOAD_STARTING = 3;
	public static final int MESSAGE_DOWNLOAD_PROGRESS = 4;
	public static final int MESSAGE_DOWNLOAD_COMPLETE = 5;
	public static final int MESSAGE_NO_USB_INTERFACE = 6;
	public static final int MESSAGE_TRAFFIC_START = 8;
	public static final int MESSAGE_TRAFFIC_COUNT = 9;
	public static final int MESSAGE_TRAFFIC_RATE = 10;
	public static final int MESSAGE_TRAFFIC_END = 11;
    public static final int MESSAGE_USB_ACTION_ATTACH = 12;
    public static final int MESSAGE_USB_ACTION_DETACH = 13;
    public static final int MESSAGE_USB_ACTION_UNKNOWN = 14;
	
	public static final String MSG_TAG = "TETHER -> MainActivity ";
	public static final String SECURE_SETTING_PATH = "/data/data/com.android.providers.settings/databases/";
	public static MainActivity currentInstance = null;

    private static void setCurrent(MainActivity current){
    	MainActivity.currentInstance = current;
    }

    private void configureLogbackDirectly() {
        // reset the default context (which may already have been initialized)
        // since we want to reconfigure it
        LoggerContext lc = (LoggerContext)LoggerFactory.getILoggerFactory();
        lc.reset();

        // setup FileAppender
        PatternLayoutEncoder encoder1 = new PatternLayoutEncoder();
        encoder1.setContext(lc);
        encoder1.setPattern("%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n");
        encoder1.start();

        FileAppender<ILoggingEvent> fileAppender = new FileAppender<ILoggingEvent>();
        fileAppender.setContext(lc);
        fileAppender.setFile(this.getFileStreamPath("SayRevTethering.log").getAbsolutePath());
        fileAppender.setEncoder(encoder1);
        fileAppender.start();

        // setup LogcatAppender
        PatternLayoutEncoder encoder2 = new PatternLayoutEncoder();
        encoder2.setContext(lc);
        encoder2.setPattern("[%thread] %msg%n");
        encoder2.start();

        LogcatAppender logcatAppender = new LogcatAppender();
        logcatAppender.setContext(lc);
        logcatAppender.setEncoder(encoder2);
        logcatAppender.start();

        // add the newly created appenders to the root logger;
        // qualify Logger to disambiguate from org.slf4j.Logger
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.addAppender(fileAppender);
        root.addAppender(logcatAppender);
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.configureLogbackDirectly();
        logger.debug("Calling onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        // Init Application
        this.application = (TetherApplication)this.getApplication();
        MainActivity.setCurrent(this);
        
        // Init Table-Rows
        this.startTblRow = (TableRow)findViewById(R.id.startRow);
        this.stopTblRow = (TableRow)findViewById(R.id.stopRow);
        this.progressBar = (ProgressBar)findViewById(R.id.progressBar);
        this.progressText = (TextView)findViewById(R.id.progressText);
        this.progressTitle = (TextView)findViewById(R.id.progressTitle);
        this.downloadUpdateLayout = (RelativeLayout)findViewById(R.id.layoutDownloadUpdate);
        
        this.trafficRow = (RelativeLayout)findViewById(R.id.trafficRow);
        this.downloadText = (TextView)findViewById(R.id.trafficDown);
        this.uploadText = (TextView)findViewById(R.id.trafficUp);
        this.downloadRateText = (TextView)findViewById(R.id.trafficDownRate);
        this.uploadRateText = (TextView)findViewById(R.id.trafficUpRate);

        // Define animation
        animation = new ScaleAnimation(
                0.9f, 1, 0.9f, 1, // From x, to x, from y, to y
                ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(600);
        animation.setFillAfter(true); 
        animation.setStartOffset(0);
        animation.setRepeatCount(1);
        animation.setRepeatMode(Animation.REVERSE);

        // enable debug logging
        //RootCommands.DEBUG = true;
        RootCommands.init(true,this.getFileStreamPath("SayRevTethering.log").getAbsolutePath());



        // Startup-Check
        if (this.application.startupCheckPerformed == false) {
	        this.application.startupCheckPerformed = true;
	    	boolean supportedKernel = false;
	        /*
	         * TODO to see if can put back this root check.
	         * DONE: Put it back because we still need the root permission to modify secure.setting database
	         * DATE: 8-July-2012
	         */
	        if (!this.application.coretask.hasRootPermission()){
	        	this.openNotRootDialog();
	        	supportedKernel = false;
	        }

        	// Checking root-permission, files
			//this.application.checkForUpdate()
	        //if (this.application.binariesExists() == false || this.application.coretask.filesetOutdated()) {
	        if (this.application.binariesExists() == false) {
	        	if (this.application.coretask.hasRootPermission()) {
	        		this.application.installFiles();
	        	}
	        }

            this.application.coretask.setSqlite3Path(this.application.coretask.hasSqlite3());

            this.application.prepareSaySU(MainActivity.this);
            this.application.coretask.setRootSu(this.application.ROOT_SU);


	        /*
	         * Add a function for get setting from settings.db to see if there is tether_supported
	         * need to write a function to dump the settings.db secure table into a file and 
	         * read from this file to see if contains tether_supported. This function is defined
	         * inside CoreTask.java
	         */
	        boolean dumpSettingSuccess = false;
	        boolean setSecureSettingFail = false;
	        /*
	         * 1. dump the secure table into table under app folder. 
	         * 
	         * 2. check this file to see:
	         * if search tether_supported in the settings.db return null, 
	         * 		insert tether_supported
	         * 		if no error, tetherIsinSettingDB = true
	         * 		else pop-up warning message
	         * 
	         * else 
	         * 		if the tether_supported value is 0
	         * 			change the value to 1
	         * 			if no error, tetherIsinSettingDB = true
	         * 			else pop-up warning message
	         *  	else
	         *  		tetherIsInSettingDB = true
	         */	
	        
	        /*
	         *  think about if it is better to put those checks into application and run in a new thread.
	         */
	        if (this.application.coretask.hasRootPermission()) {
	        	if ((this.application.dumpSecureSettings()==false)||(this.application.dumpSecureSettingsMaxID()==false)){
	        		this.openFailToDumpSecureSettingDialog();
	        		dumpSettingSuccess = false;
	        		}
	        	else  dumpSettingSuccess = true;
	        }	
	        
	        int iMaxId = -1;
			if(!this.application.coretask.checkSecureSetting(this.application.coretask.secureSetting_tether_supported)){
				iMaxId = this.application.coretask.secureSettingMaxId();
				if (iMaxId < 0){   //error occurs during retrieve maxId of secure table, return -1 if error
                    logger.debug("cannot read system setting's max id ...");
					this.application.coretask.appendLog(MSG_TAG+ "cannot read system setting's max id during checking tether_supported...");
				}
				else {	//if there is no error to retrieve maxId, then insert with new name field and maxid
					iMaxId = iMaxId + 1;
					if(!this.application.coretask.secureSettingInsertandEnable(this.application.coretask.secureSetting_tether_supported, iMaxId, 1)){
						setSecureSettingFail = true;
						logger.debug("cannot insert and enable " + this.application.coretask.secureSetting_tether_supported );
						this.application.coretask.appendLog(MSG_TAG + "cannot insert and enable " + this.application.coretask.secureSetting_tether_supported );
					}
				}
			}
			else if(!this.application.coretask.secureSettingIsEnabled(this.application.coretask.secureSetting_tether_supported, 1)){
				if(!this.application.coretask.secureSettingEnable(this.application.coretask.secureSetting_tether_supported, 1)){
					setSecureSettingFail = true;
					logger.debug("cannot enable " + this.application.coretask.secureSetting_tether_supported);
					this.application.coretask.appendLog(MSG_TAG + "cannot enable " + this.application.coretask.secureSetting_tether_supported);
				}
			}
			else {
				logger.debug(this.application.coretask.secureSetting_tether_supported + " has been enabled already");
				this.application.coretask.appendLog(MSG_TAG + this.application.coretask.secureSetting_tether_supported + " has been enabled already");
			}
			
			
			
			if(!this.application.coretask.checkSecureSetting(this.application.coretask.secureSetting_tether_dun_required)){
				//int iMaxId = this.application.coretask.secureSettingMaxId();
				if (iMaxId < 0){   //error occurs during retrieve maxId of secure table
					logger.debug("cannot read system setting's max id ...");
					this.application.coretask.appendLog(MSG_TAG + "cannot read system setting's max id during checking tether_dun_required...");
				}
				else {	//if there is no error to retrieve maxId, then insert with new name field and maxid
					iMaxId = iMaxId + 1;
					if(!this.application.coretask.secureSettingInsertandEnable(this.application.coretask.secureSetting_tether_dun_required, iMaxId, 0)){
						setSecureSettingFail = true;
						logger.debug("cannot insert and enable " + this.application.coretask.secureSetting_tether_dun_required );
						this.application.coretask.appendLog(MSG_TAG + "cannot insert and enable " + this.application.coretask.secureSetting_tether_dun_required );
					}
				}
			}
			else if(!this.application.coretask.secureSettingIsEnabled(this.application.coretask.secureSetting_tether_dun_required, 0)){
				if(!this.application.coretask.secureSettingEnable(this.application.coretask.secureSetting_tether_dun_required, 0)){
					setSecureSettingFail = true;
					logger.debug("cannot enable " + this.application.coretask.secureSetting_tether_dun_required);
					this.application.coretask.appendLog(MSG_TAG + "cannot enable " + this.application.coretask.secureSetting_tether_dun_required);
				}
			}
			else {
				logger.debug(this.application.coretask.secureSetting_tether_dun_required + " has been enabled already");
				this.application.coretask.appendLog(MSG_TAG + this.application.coretask.secureSetting_tether_dun_required + " has been enabled already");
			}
	        
			
			this.application.coretask.appendLog(MSG_TAG + "dumpSettingSuccess is:" + dumpSettingSuccess);
			this.application.coretask.appendLog(MSG_TAG + "setSecureSettingFail is:" + setSecureSettingFail);
			if(dumpSettingSuccess && !setSecureSettingFail){
				
				//Use reflection to retrieve the isTetheringSupported method from connnectivityManager
				ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
				Method isTetheringSupportedLocal = null;
				Method getTetherableUsbRegexsLocal = null;  //added temp
				String [] tetherRegex;
				try {
					isTetheringSupportedLocal = cm.getClass().getMethod("isTetheringSupported");
					getTetherableUsbRegexsLocal = cm.getClass().getMethod("getTetherableUsbRegexs");
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
	    	
				//Use this high level general method to make judgment
				try {
					supportedKernel = (Boolean) isTetheringSupportedLocal.invoke(cm);
					tetherRegex = (String []) getTetherableUsbRegexsLocal.invoke(cm);
					this.application.coretask.appendLog(MSG_TAG + "supportedKernel value is:" + supportedKernel);
					
					//note: cannot use (boolean) isTetheringSupportedLocal.invoke(cm);
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	
				if (!supportedKernel)
					this.openUnsupportedKernelDialog();
	        
				}
	


	        // Open donate-dialog
			this.openDonateDialog();
			
			//TODO: try not use getBaseContext()
			//Context mContext= getBaseContext();
			Context mContext = MainActivity.this;
			
			//TODO: those lines below for debug only
			int tetherEnabledInSettings = 0;
			try {
				tetherEnabledInSettings = Settings.Global.getInt(mContext.getContentResolver(),
						"tether_supported");
			} catch (SettingNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.debug("tetherEnabledInSettings is ..." + tetherEnabledInSettings);
			// Check for updates  commented out for now 13May2012
			//TODO: comment out for now (23Jun2012)
			//this.application.checkForUpdate();
        }
       
       
        //setup computer side textview button listener for help button
        this.setupComputer = (Button) findViewById(R.id.setupComputer);
        logger.debug("SetupPChelp to be pressed ...");
        setupComputer.setOnClickListener(new setupComputerListener());

        newStartTetherFragment = TetherActionDialogFragment.newInstance(MainActivity.ID_DIALOG_STARTING);
        newStopTetherFragment = TetherActionDialogFragment.newInstance(MainActivity.ID_DIALOG_STOPPING);

        // Start Button
        this.startBtn = (ImageView) findViewById(R.id.startTetherBtn);
		this.startBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				logger.debug("StartBtn pressed ...");
		    	//showDialog(MainActivity.ID_DIALOG_STARTING);
                newStartTetherFragment.show(getSupportFragmentManager(), "startTether");
                newStartTetherFragment.setCancelable(false);
				new Thread(new Runnable(){
					public void run(){
                        synchronized (this) {
						int started = MainActivity.this.application.startTether();
						logger.debug("startTether excuted and the returen value is ..."+started);
						MainActivity.this.application.coretask.appendLog("startTether excuted and the returen value is: " + started );
						//return 0 if start ok, return 2 if problem
						//MainActivity.this.dismissDialog(MainActivity.ID_DIALOG_STARTING);
                        newStartTetherFragment.dismiss();
						Message message = Message.obtain();
						if (started != 0) {
							message.what = started;
						}
						MainActivity.this.viewUpdateHandler.sendMessage(message); 
					}
                    }
				}).start();
			}
		});

		// Stop Button
		this.stopBtn = (ImageView) findViewById(R.id.stopTetherBtn);
		this.stopBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				logger.debug("StopBtn pressed ...");
		    	//showDialog(MainActivity.ID_DIALOG_STOPPING);
                newStopTetherFragment.show(getSupportFragmentManager(), "stopTether");
                newStopTetherFragment.setCancelable(false);
				new Thread(new Runnable(){
					public void run(){
						MainActivity.this.application.stopTether();
						//MainActivity.this.dismissDialog(MainActivity.ID_DIALOG_STOPPING);
                        newStopTetherFragment.dismiss();
						MainActivity.this.viewUpdateHandler.sendMessage(new Message()); 
					}
				}).start();
			}
		});			
		this.toggleStartStop();

        //this.detectUsbDeviceAttach();
    }

    public static class TetherActionDialogFragment extends DialogFragment {
        int mNum;
        /**
         * Create a new instance of TetherActionDialogFragment, providing "num"
         * as an argument.
         */
        static TetherActionDialogFragment newInstance(int num) {
            TetherActionDialogFragment f = new TetherActionDialogFragment();

            // Supply num input as an argument.
            Bundle args = new Bundle();
            args.putInt("num", num);
            f.setArguments(args);

            return f;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            super.onCreateDialog(savedInstanceState);
            mNum = getArguments().getInt("num");
            // Use the Builder class for convenient dialog construction
            //AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            ProgressDialog.Builder builder = new ProgressDialog.Builder(getActivity());
            if(mNum == 0) {
                // Set the dialog title
                builder.setTitle("Start Tethering");
                builder.setMessage("Please wait while starting...");
                builder.setCancelable(false);
                // Create the AlertDialog object and return it
                return builder.create();
            } else if(mNum == 1) {
                // Set the dialog title
                builder.setTitle("Stop Tethering");
                builder.setMessage("Please wait while stopping...");
                builder.setCancelable(false);
                // Create the AlertDialog object and return it
                return builder.create();
            }
            return null;
        }

        public Dialog onCreateDialog1(Bundle savedInstanceState) {
            super.onCreateDialog(savedInstanceState);
            mNum = getArguments().getInt("num");
            // Use the Builder class for convenient dialog construction
            //AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            ProgressDialog progressDialog = new ProgressDialog(getActivity());
            if(mNum == 0) {
                // Set the dialog title
                progressDialog.setTitle("Start Tethering");
                progressDialog.setMessage("Please wait while starting...");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(false);
                // Create the AlertDialog object and return it
                return progressDialog;
            } else if(mNum == 1) {
                // Set the dialog title
                progressDialog.setTitle("Stop Tethering");
                progressDialog.setMessage("Please wait while stopping...");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(false);
                // Create the AlertDialog object and return it
                return progressDialog;
            }
            return null;
        }

        public void setIndeterminate(boolean enable){
            ((ProgressDialog)getDialog()).setIndeterminate(enable);
        }

        public static void showLoadingProgress(FragmentManager manager) {
            dismissLoadingProgress(manager);
            TetherActionDialogFragment loading = new TetherActionDialogFragment();
            loading.show(manager, "loading");
        }

        public static void dismissLoadingProgress(FragmentManager manager) {
            FragmentTransaction tr = manager.beginTransaction();
            Fragment frag = manager.findFragmentByTag("loading");
            if(frag != null){
                tr.remove(frag);
            }
            tr.commit();
        }
    }
	
    
    class setupComputerListener implements OnClickListener {
    	public void onClick(View v) {
          // do something when the button is clicked
        	logger.debug("SetupPChelp pressed ...");
        	//Toast.makeText(v.getContext(), "Im Clicked!", Toast.LENGTH_SHORT).show();
    		Intent i = new Intent(MainActivity.this, SetupPCHelpActivity.class);
    		MainActivity.this.startActivity(i);
        }
    }
 
    
	public void onStop() {
    	logger.debug("Calling onStop()");
		super.onStop();
	}

	public void onDestroy() {
    	logger.debug("Calling onDestroy()");
    	super.onDestroy();
    	//TODO: check if need to kill some function like defined in the tethering
        //unregisterReceiver(UsbConnectionReceiver);


	}

	public void onResume() {
		logger.debug("Calling onResume()");
		//this.showRadioMode();
        super.onResume();
        /*Intent intent = getIntent();
        String curItentActionName = intent.getAction();
        Toast.makeText(getApplicationContext(), String.valueOf("Into onResume"), 1000).show();
        Toast.makeText(getApplicationContext(), String.valueOf("curItentActionName=" + curItentActionName), 1000).show();
        Toast.makeText(getApplicationContext(), String.valueOf("bUsbConnected=" + application.usbActionDetectConnected), 1000).show();

        if (this.application.usbActionDetectConnected==false ) {
            //check to see if USB is now connected
        }*/
	}
	
	private static final int MENU_SETUP = 0;
	private static final int MENU_LOG = 1;
	private static final int MENU_ABOUT = 2;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	boolean supRetVal = super.onCreateOptionsMenu(menu);
    	SubMenu setup = menu.addSubMenu(0, MENU_SETUP, 0, getString(R.string.setuptext));
    	setup.setIcon(drawable.ic_menu_preferences);
    	SubMenu log = menu.addSubMenu(0, MENU_LOG, 0, getString(R.string.logtext));
    	log.setIcon(drawable.ic_menu_agenda);
    	SubMenu about = menu.addSubMenu(0, MENU_ABOUT, 0, getString(R.string.abouttext));
    	about.setIcon(drawable.ic_menu_info_details);      	
    	return supRetVal;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
    	boolean supRetVal = super.onOptionsItemSelected(menuItem);
    	logger.debug("Menuitem:getId  -  "+menuItem.getItemId()); 
    	switch (menuItem.getItemId()) {
	    	case MENU_SETUP :
		        startActivityForResult(new Intent(
		        		MainActivity.this, SetupActivity.class), 0);
		        break;
	    	case MENU_LOG :
		        startActivityForResult(new Intent(
		        		MainActivity.this, LogActivity.class), 0);
		        break;
	    	case MENU_ABOUT :
	    		this.openAboutDialog();
    	}
    	return supRetVal;
    }    

    @Override
    protected Dialog onCreateDialog(int id) {
    	if (id == ID_DIALOG_STARTING) {
	    	progressDialog = new ProgressDialog(this);
	    	progressDialog.setTitle("Start Tethering");
	    	progressDialog.setMessage("Please wait while starting...");
	    	progressDialog.setIndeterminate(false);
	    	progressDialog.setCancelable(true);
	        return progressDialog;
    	}
    	else if (id == ID_DIALOG_STOPPING) {
	    	progressDialog = new ProgressDialog(this);
	    	progressDialog.setTitle("Stop Tethering");
	    	progressDialog.setMessage("Please wait while stopping...");
	    	progressDialog.setIndeterminate(false);
	    	progressDialog.setCancelable(true);
	        return progressDialog;  		
    	}
    	return null;
    }

    public Handler viewUpdateHandler = new Handler(){
        public void handleMessage(Message msg) {
        	switch(msg.what) {
        	case MESSAGE_NO_DATA_CONNECTION :
        		logger.debug("No mobile-data-connection established!");
        		MainActivity.this.application.displayToastMessage("No mobile-data-connection established!");
            	MainActivity.this.toggleStartStop();
            	break;
        	case MESSAGE_CANT_START_TETHER :
        		logger.debug("Unable to start tethering!");
        		MainActivity.this.application.displayToastMessage("Unable to start tethering. Please try again!");
            	MainActivity.this.toggleStartStop();
            	break;
        	case MESSAGE_TRAFFIC_START :
        		MainActivity.this.trafficRow.setVisibility(View.VISIBLE);
        		break;
        	case MESSAGE_TRAFFIC_COUNT :
        		MainActivity.this.trafficRow.setVisibility(View.VISIBLE);
	        	long uploadTraffic = ((TetherApplication.DataCount)msg.obj).totalUpload;
	        	long downloadTraffic = ((TetherApplication.DataCount)msg.obj).totalDownload;
	        	long uploadRate = ((TetherApplication.DataCount)msg.obj).uploadRate;
	        	long downloadRate = ((TetherApplication.DataCount)msg.obj).downloadRate;

	        	// Set rates to 0 if values are negative
	        	if (uploadRate < 0)
	        		uploadRate = 0;
	        	if (downloadRate < 0)
	        		downloadRate = 0;
	        	
        		MainActivity.this.uploadText.setText(MainActivity.this.formatCount(uploadTraffic, false));
        		MainActivity.this.downloadText.setText(MainActivity.this.formatCount(downloadTraffic, false));
        		MainActivity.this.downloadText.invalidate();
        		MainActivity.this.uploadText.invalidate();

        		MainActivity.this.uploadRateText.setText(MainActivity.this.formatCount(uploadRate, true));
        		MainActivity.this.downloadRateText.setText(MainActivity.this.formatCount(downloadRate, true));
        		MainActivity.this.downloadRateText.invalidate();
        		MainActivity.this.uploadRateText.invalidate();
        		break;
        	case MESSAGE_TRAFFIC_END :
        		MainActivity.this.trafficRow.setVisibility(View.INVISIBLE);
        		break;
        	case MESSAGE_DOWNLOAD_STARTING :
        		logger.debug("Start progress bar");
        		MainActivity.this.progressBar.setIndeterminate(true);
        		MainActivity.this.progressTitle.setText((String)msg.obj);
        		MainActivity.this.progressText.setText("Starting...");
        		MainActivity.this.downloadUpdateLayout.setVisibility(View.VISIBLE);
        		break;
        	case MESSAGE_DOWNLOAD_PROGRESS :
        		MainActivity.this.progressBar.setIndeterminate(false);
        		MainActivity.this.progressText.setText(msg.arg1 + "k /" + msg.arg2 + "k");
        		MainActivity.this.progressBar.setProgress(msg.arg1*100/msg.arg2);
        		break;
        	case MESSAGE_DOWNLOAD_COMPLETE :
        		logger.debug("Finished download.");
        		MainActivity.this.progressText.setText("");
        		MainActivity.this.progressTitle.setText("");
        		MainActivity.this.downloadUpdateLayout.setVisibility(View.GONE);
        		break;
        	case MESSAGE_NO_USB_INTERFACE :
        		logger.debug("No tetherable USB interface!");
        		MainActivity.this.application.displayToastMessage("No tetherable USB interface!");
            	MainActivity.this.toggleStartStop();
            	break;
            case MESSAGE_USB_ACTION_UNKNOWN :
                logger.debug("Unkown USB action !");
                MainActivity.this.application.displayToastMessage("Unkown USB action !");
                //MainActivity.this.toggleStartStop();
                break;
            case MESSAGE_USB_ACTION_ATTACH :
                logger.debug("Attach USB action !");
                MainActivity.this.application.displayToastMessage("Now should call the HART auto scan");
                //MainActivity.this.toggleStartStop();
                // break;
            case MESSAGE_USB_ACTION_DETACH :
                logger.debug("Detach USB action !");
                MainActivity.this.application.displayToastMessage("Not connected to PC!");
                MainActivity.this.toggleStartStop();
                break;
        	default:
        		//debug here with appendlog
        		MainActivity.this.application.coretask.appendLog("default message to excute toggleStartStop" );
        		MainActivity.this.toggleStartStop();
        	}
        	super.handleMessage(msg);
        	System.gc();
        }
   };

   private void toggleStartStop() {
    	//jason-9Apri2012, for reverse tethering, you don't need dnsmaqs running.
	   // since we are not going to use either dnsmasq or nat (iptables), we have to choose
	   // other thing to make judgment. from tetherStartStop.cpp, we know that the only 
	   //way is using the statements inside start/stop int functions.
	   //even for motorola defy, 
	   //sys/devices/virtual/usb_composite/rndis/enable 
	   //will be set to 1, so we can create a function to replace the 
	   // dnsmasq and nat condition judgments.
	    //boolean dnsmasqRunning = false;
	    //try {
		//	dnsmasqRunning = this.application.coretask.isProcessRunning("bin/dnsmasq");
		//} catch (Exception e) {
		//	MainActivity.this.application.displayToastMessage("Unable to check if dnsmasq is currently running!");
		//}
    	
		
    	
	   //TODO:need to rewrite this method to use android api
	   //boolean usbNetRunning = this.application.coretask.isUsbNetEnabled();
	   
	   boolean usbNetRunning = false;
	   /*ConnectivityManager cm =
           (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
	   Method getTetherableIfacesLocal = null;
		try {
			getTetherableIfacesLocal = cm.getClass().getMethod("getTetheredIfaces");
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String[] tethered = null;
    	try {
			tethered = (String [])getTetherableIfacesLocal.invoke(cm);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	String usbIface = MainActivity.this.application.findIface(tethered, MainActivity.this.application.mUsbRegexs);		
    	this.application.coretask.appendLog("tethered interface is " + usbIface);
       
    	if (usbIface == null) {
           usbNetRunning = false;
        }*/
    	
    	if ((MainActivity.this.application.tetherStarted == MainActivity.this.application.TETHER_ERROR_NO_ERROR) && (MainActivity.this.application.tetherStopped ==-1)){
    		usbNetRunning = true;
    	}
    		
    	
	   //if (dnsmasqRunning == true && natEnabled == true){
    	if (usbNetRunning == true){	
    		this.startTblRow.setVisibility(View.GONE);
    		this.stopTblRow.setVisibility(View.VISIBLE);
    		// Animation
    		if (this.animation != null)
    			this.stopBtn.startAnimation(this.animation);
    		// Notification
    		this.application.tetherNetworkDevice = MainActivity.this.application.usbIface;//jason change to usb1
    		this.application.trafficCounterEnable(true);
    		this.application.showStartNotification();
    	}
    	else if (usbNetRunning == false) {
    		this.startTblRow.setVisibility(View.VISIBLE);
    		this.stopTblRow.setVisibility(View.GONE);
    		this.application.trafficCounterEnable(false);
    		// Animation
    		if (this.animation != null)
    			this.startBtn.startAnimation(this.animation);
    		// Notification
        	this.application.notificationManager.cancelAll();
    	}   	
    	else {
    		this.startTblRow.setVisibility(View.VISIBLE);
    		this.stopTblRow.setVisibility(View.VISIBLE);
    		MainActivity.this.application.displayToastMessage("Your phone is currently in an unknown state - try to reboot!");
    	}
    	//this.showRadioMode();
    	System.gc();
    }
   
	private String formatCount(long count, boolean rate) {
		// Converts the supplied argument into a string.
		// 'rate' indicates whether is a total bytes, or bits per sec.
		// Under 2Mb, returns "xxx.xKb"
		// Over 2Mb, returns "xxx.xxMb"
		if (count < 1e6 * 2)
			return ((float)((int)(count*10/1024))/10 + (rate ? "kbps" : "kB"));
		return ((float)((int)(count*100/1024/1024))/100 + (rate ? "mbps" : "MB"));
	}
  
   	private void openUnsupportedKernelDialog() {
		LayoutInflater li = LayoutInflater.from(this);
        View view = li.inflate(R.layout.unsupportedkernelview, null); 
		new AlertDialog.Builder(MainActivity.this)
        .setTitle("Unsupported Kernel!")
        .setView(view)
        .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                        logger.debug("Close pressed");
                        MainActivity.this.finish();
                }
        })
        .setNeutralButton("Ignore", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    logger.debug("Override pressed");
                    //TODO: to see if it is needed 23Jun2012 jason GONG
                    //MainActivity.this.application.installFiles();
                    MainActivity.this.application.displayToastMessage("Ignoring, note that tethering will NOT work.");
                }
        })
        .show();
   	}
   	
   	private void openNotRootDialog() {
		LayoutInflater li = LayoutInflater.from(this);
        View view = li.inflate(R.layout.norootview, null); 
		new AlertDialog.Builder(MainActivity.this)
        .setTitle("Not Root!")
        .setView(view)
        .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                        logger.debug("Close pressed");
                        MainActivity.this.finish();
                }
        })
        .setNeutralButton("Override", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    logger.debug("Override pressed");
					//TODO: to see if it is really needed 23jun2012 jason GONG
                    //MainActivity.this.application.installFiles();
                }
        })
        .show();
   	}
   
   	private void openAboutDialog() {
		LayoutInflater li = LayoutInflater.from(this);
        View view = li.inflate(R.layout.aboutview, null); 
        TextView versionName = (TextView)view.findViewById(R.id.versionName);
        versionName.setText(this.application.getVersionName());        
		new AlertDialog.Builder(MainActivity.this)
        .setTitle("About")
        .setView(view)
        .setNeutralButton("Top Apps", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                        logger.debug("Top Apps pressed");
                        String url = "http://ad.leadboltads.net/show_app_wall?section_id=Nothing"; //YOUR LEADBOLT APPWALL ADRESS HERE!!
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(intent);
    					//Uri uri = Uri.parse(getString(R.string.paypalUrl));
    					//startActivity(new Intent(Intent.ACTION_VIEW, uri));
                }
        })
        .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                        logger.debug("Close pressed");
                }
        })
        .show();  		
   	}
   	
   	private void openDonateDialog() {
   		if (this.application.showDonationDialog()) {
   			// Disable donate-dialog for later startups
   			this.application.preferenceEditor.putBoolean("donatepref", false);
   			this.application.preferenceEditor.commit();
   			// Creating Layout
			LayoutInflater li = LayoutInflater.from(this);
	        View view = li.inflate(R.layout.donateview, null); 
	        new AlertDialog.Builder(MainActivity.this)
	        .setTitle("Donate")
	        .setView(view)
	        .setNeutralButton("Close", new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int whichButton) {
	                        logger.debug("Close pressed");
	                }
	        })
	        .setNegativeButton("Top Apps", new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int whichButton) {
	                        logger.debug("Top Apps pressed");
	                        String url = "http://ad.leadboltads.net/show_app_wall?section_id=989407946"; //YOUR LEADBOLT APPWALL ADRESS HERE!!
	                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
	                        startActivity(intent);
	    					//Uri uri = Uri.parse(getString(R.string.paypalUrl));
	    					//startActivity(new Intent(Intent.ACTION_VIEW, uri));
	                }
	        })
	        .show();
   		}
   	}
   	
   	public void openUpdateDialog(final String downloadFileUrl, final String fileName) {
		LayoutInflater li = LayoutInflater.from(this);
        View view = li.inflate(R.layout.updateview, null); 
		new AlertDialog.Builder(MainActivity.this)
        .setTitle("Update Application?")
        .setView(view)
        .setNeutralButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                	logger.debug("No pressed");
                }
        })
        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    logger.debug("Yes pressed");
                    MainActivity.this.application.downloadUpdate(downloadFileUrl, fileName);
                }
        })
        .show();
   	}
   	
   	public void openFailToDumpSecureSettingDialog() {
		LayoutInflater li = LayoutInflater.from(this);
        View view = li.inflate(R.layout.faildumpsecureview, null); 
		new AlertDialog.Builder(MainActivity.this)
        .setTitle("Fail Get Sys Setting!")
        .setView(view)
        .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                        logger.debug("Close pressed");
                        MainActivity.this.finish();
                }
        })
         .setNeutralButton("Ignore", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    logger.debug("Override pressed");
                    //TODO: to see if it is needed 23Jun2012 jason GONG
                    //MainActivity.this.application.installFiles();
                    MainActivity.this.application.displayToastMessage("Ignoring, note that tethering will NOT work.");
                }
        })
        .show();
   	}
 
  	public void openNoUSBIfaceDialog() {
		LayoutInflater li = LayoutInflater.from(this);
        View view = li.inflate(R.layout.nousbifaceview, null); 
		new AlertDialog.Builder(MainActivity.this)
        .setTitle("Fail TO Get USB Interface!")
        .setView(view)
        .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                        logger.debug("Close pressed");
                        MainActivity.this.finish();
                }
        })
         .setNeutralButton("Ignore", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    logger.debug("Ignore USB failure pressed");
                    //TODO: to see if it is needed 23Jun2012 jason GONG
                    //MainActivity.this.application.installFiles();
                    MainActivity.this.application.displayToastMessage("Ignoring, note that tethering will NOT work.");
                }
        })
        .show();
   	}   	
   	
   	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
          //  builder.setTitle("Test");  IF YOU WANT TO SET SEPARATE TITLE
          //  builder.setIcon(R.drawable.ic_launcher); // ICON FOR YOUR ALERTDIALOG
            builder.setCancelable(false); // disable back-button
            builder
                    .setMessage("Do you want to Quit?");
            builder.setPositiveButton("Top Apps",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        String url = "http://ad.leadboltads.net/show_app_wall?section_id=989407946"; //YOUR LEADBOLT APPWALL ADRESS HERE!!
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            startActivity(intent);
                        }
                    });
            builder.setNeutralButton("Quit", //Quit-button
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        	if ((MainActivity.this.application.tetherStarted == MainActivity.this.application.TETHER_ERROR_NO_ERROR) && (MainActivity.this.application.tetherStopped ==-1)){
                        	MainActivity.this.application.displayToastMessage("Please stop tethering first!");	
                        	//MainActivity.this.application.stopTether();
                        	//MainActivity.this.viewUpdateHandler.sendMessage(new Message());
                        	dialog.cancel();
                        	}
                        	else{
                        		finish();
                        	}
                        }
                    });
            builder.setNegativeButton("Cancel", //Cancel-button
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            builder.show();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    // Monitor Changes in USB Connecting State
    public BroadcastReceiver UsbConnectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String curItentActionName = intent.getAction();
            if(UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(curItentActionName)){
                Toast.makeText(context, String.valueOf("Found USB Device attached +++DEVICE+++ !"), 1000).show();

                Toast.makeText(context, String.valueOf("Component: " + intent.getComponent()), 1000).show();
                Toast.makeText(context, String.valueOf("Aciton: " +  intent.getAction()), 1000).show();
                Toast.makeText(context, String.valueOf("Categories: " +  intent.getCategories()), 1000).show();
                Toast.makeText(context, String.valueOf("Data: " + intent.getData()), 1000).show();
                Toast.makeText(context, String.valueOf("DataType: " + intent.getType()), 1000).show();
                Toast.makeText(context, String.valueOf("DataSchema: " + intent.getScheme()), 1000).show();

                UsbManager manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
                HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
                Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
                while(deviceIterator.hasNext()){
                    UsbDevice device = deviceIterator.next();
                    int usbVid = device.getVendorId();
                    int usbPid = device.getProductId();
                    //if((usbVid == ft232rUartVid) && (usbPid ==ft232rUartPid) ){
                    if(true) {
                        MainActivity.currentInstance.application.usbActionDetectConnected = true;
                        Toast.makeText(context, "Found Usb device: ", Toast.LENGTH_LONG).show();
                        break;
                    }
                }
            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(curItentActionName)){
                MainActivity.currentInstance.application.usbActionDetectConnected = false;
                Toast.makeText(context, String.valueOf("Found USB Device detached ---DEVICE--- !"), 1000).show();
            } else if (UsbManager.ACTION_USB_ACCESSORY_ATTACHED.equals(curItentActionName)){
                Toast.makeText(context, String.valueOf("Found USB accessory attached +++ACCESSORY+++ !"), 1000).show();
            } else if (UsbManager.ACTION_USB_ACCESSORY_DETACHED.equals(curItentActionName)){
                Toast.makeText(context, String.valueOf("Found USB accessory detached ---ACCESSORY---!"), 1000).show();
            } else {
                Toast.makeText(context, String.valueOf("Unrecoginzed action !"), 1000).show();
            }
        }
    };
    public void detectUsbDeviceAttach() {
        //IntentFilter filter = new IntentFilter();
        //filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        //filter.addAction(Intent.ACTION_MEDIA_CHECKING);
        //filter.addAction(Intent.ACTION_MEDIA_EJECT);
        //filter.addAction(Intent.ACTION_MEDIA_REMOVED);
        // avoid to  can not receive broadcast
        //filter.addDataScheme("file");
        IntentFilter usbDeviceStateFilter = new IntentFilter();
        usbDeviceStateFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        usbDeviceStateFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        usbDeviceStateFilter.addAction(UsbManager.ACTION_USB_ACCESSORY_ATTACHED);
        usbDeviceStateFilter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
        usbDeviceStateFilter.addDataScheme("file");
        //usbDeviceStateFilter.addAction(ACTION_USB_PERMISSION);
        //UsbDeviceStateReceiver usbDeviceStateReceiver = new UsbDeviceStateReceiver();
        //registerReceiver(usbDeviceStateReceiver, usbDeviceStateFilter);
        registerReceiver(UsbConnectionReceiver, usbDeviceStateFilter);
        //MainActivity.currentInstance.registerReceiver(MainActivity.currentInstance.UsbConnectionReceiver, usbDeviceStateFilter);
    }

    public boolean isUsbPlugged() {
        Intent intent = MainActivity.this.registerReceiver(null,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        if (plugged == BatteryManager.BATTERY_PLUGGED_USB) {
            return true;
        } else {
            return false;
        }
    }
}

